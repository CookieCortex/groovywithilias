# README #

### What is this repository for? ###

* A console interface to communicate with the Ilias Server of the Hochschule Karlsruhe.
* You can use a simple DSL to execute common tasks, which are difficult to do on the GUI.
* You need an IZ-account in order to use this client.

### How do I get set up? ###

1. Clone the repository from the Hochschul-Gitlab-Server.
2. Add all three libraries in the lib-folder as libraries: Right click on the jar -> Add as library
3. Via Plugins->Browse repositories-> ANTLR v4 grammar plugin
   add the ANTLR Plugin to generate the needed classes for the proprocessing.
4. Right click on the Grammar.g4-file -> Configure ANTLR
	- "Output directory where all output is generated:" *PathToProject\groovywithilias\src*
	- "Package/namespace for the generated code:" *preprocessing.grammar*
	- Check "Generate parser tree visitor"
5. Under File -> Project Structure -> Artifacts create a new artifct (jar-file) via
   -> JAR -> From modules with dependencies erstellen. The main class is *main.MainGroovy*.
	- Under Windows you have to remove the *svg-salamander-x.x.jar* in the tab "Output Layout".
6. You can build the project via Build -> Build Artifacts. The jar-file is in *out\artifacts\groovywithilias_jar*.
7. You can execute the jar with *java -jar .\groovywithilias.jar .\my_script.groovy*.
