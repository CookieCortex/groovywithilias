package preprocessing

import main.Log
import org.antlr.v4.runtime.CharStream
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.tree.ParseTree
import preprocessing.grammar.GrammarLexer
import preprocessing.grammar.GrammarParser

class Preprocessor {

    /**
     * Pre processes a given processedFile, aka the script. It also checks for syntactic and semantic errors
     * and returns null if at least one occurred.
     * Processing steps:
     *      Adds parentheses to methods with no parameter.
     *      Escapes key characters in the password.
     *      Transforms custom regex into valid regex.
     * @param input The script
     * @return The pre processed script or null if at least one syntactic or semantic error occurred
     */
    static String preprocessFile(File input) {
        //Converting the processedFile in order to make it processable by ANTLR.
        CharStream charStream = CharStreams.fromStream(new FileInputStream(input))
        GrammarLexer lexer = new GrammarLexer(charStream)
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer))
        ParseTree tree = parser.operation()
        int numberOfSyntaxErrors = parser.getNumberOfSyntaxErrors()
        //TODO: This doesn't seem to catch all errors
        if (numberOfSyntaxErrors > 0) {
            Log.e("You have $numberOfSyntaxErrors syntax error. Please resolve them and run the script again.")
            return null
        }
        //Creating custom visitor to traverse the built tree
        DSLVisitor visitor = new DSLVisitor()
        boolean semanticErrorOccurred = visitor.visit(tree)
        if(semanticErrorOccurred) {
            Log.e("At least one semantic error occurred. Please fix it an restart the script again.")
            return null
        }
        //Retrieving the processed processedFile
        String preprocessedFile = visitor.getFile()
        Log.d("Pre processed input processedFile to:\n")
        Log.d(preprocessedFile)
//        Log.d()
        return preprocessedFile
    }
}
