package preprocessing

import main.Log
import org.antlr.v4.runtime.tree.RuleNode
import preprocessing.grammar.GrammarBaseVisitor
import preprocessing.grammar.GrammarParser

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDateTime

/**
 * A class to traverse a given {@link org.antlr.v4.runtime.tree.ParseTree}.
 * If one node (its representative method) computes a syntactic or semantic error it returns
 * true, therefore stops all other siblings and following nodes to be visited.
 */
class DSLVisitor extends GrammarBaseVisitor<Boolean> {

    private StringBuilder processedFile

    DSLVisitor() {
        processedFile = new StringBuilder()
    }

    /**
     * Describes the start value when visiting all
     * children. Per default no error has occurred yet.
     * @return false. No error has occurred at the start
     */
    @Override
    protected Boolean defaultResult() {
        return false
    }

    /**
     *
     * @param aggregate Describes the aggregate of the result of all pre siblings of the current node
     * @param nextResult Did an error occur in the current node?
     * @return Has overall an error occurred?
     */
    @Override
    protected Boolean aggregateResult(Boolean aggregate, Boolean nextResult) {
        return aggregate | nextResult
    }

    /**
     * As soon as an error occurred we stop visiting following siblings.
     * @param node The current node
     * @param hasYetAnErrorOccurred Has yet an error occurred?
     * @return true if no error has yet occurred an we can visit the next children
     */
    @Override
    protected boolean shouldVisitNextChild(RuleNode node, Boolean hasYetAnErrorOccurred) {
        return !hasYetAnErrorOccurred
    }

    @Override
    Boolean visitUser(GrammarParser.UserContext ctx) {
        //Simply add children to file.
        processedFile.append(ctx.getChild(0)).append(" ").append(ctx.getChild(1)).append("\n")
        return visitChildren(ctx)
    }


    @Override
    Boolean visitPw(GrammarParser.PwContext ctx) {
        //Escape key characters and add children to file.
        String refactoredPassword = Refactory.refactorPassword(ctx.getChild(1).toString())
        processedFile.append(ctx.getChild(0)).append(" ").append(refactoredPassword).append("\n")
        return visitChildren(ctx)
    }

    @Override
    Boolean visitCourse(GrammarParser.CourseContext ctx) {
        //Simply add children to file without line break.
        processedFile.append(ctx.getChild(0)).append(" ").append(ctx.getChild(1)).append(" ")
        return visitChildren(ctx)
    }

    @Override
    Boolean visitGroup(GrammarParser.GroupContext ctx) {
        //Transform pseudo regex to valid ones and add children to file.
        processedFile.append(ctx.getChild(0)).append(" ")
        for (int count = 1; count < ctx.getChildCount(); ++count) {
            String refactoredGroupElement = ctx.getChild(count).toString()
            if (count % 2 == 1) {
                //If count is even we have the comma separator which doesn't have to be refactored.
                refactoredGroupElement = Refactory.refactorGroupElement(ctx.getChild(count).toString())
                if (refactoredGroupElement == null) {
                    //Error occurred while processing the regex
                    return true
                }
            }
            processedFile.append(refactoredGroupElement).append(" ")
        }
        return visitChildren(ctx)
    }

    @Override
    Boolean visitAddGroup(GrammarParser.AddGroupContext ctx) {
        //Transform pseudo regex to valid ones and add children to file.
        processedFile.append(ctx.getChild(0)).append(" ")
        for (int count = 1; count < ctx.getChildCount(); ++count) {
            if (count % 2 == 1) {
                //If count is even we have the comma separator which doesn't have to be refactored.
                ArrayList<String> refactoredAddGroupElementResult = Refactory.refactorAddGroupElement(ctx.getChild(count).toString())
                if (refactoredAddGroupElementResult == null) {
                    //Error occurred while processing the regex
                    return true
                }
                for (String element : refactoredAddGroupElementResult) {
                    processedFile.append(element).append(",")
                }
                processedFile.deleteCharAt(processedFile.size() - 1).append(" ")
            } else {
                processedFile.append(ctx.getChild(count).toString()).append(" ")
            }
        }
        return visitChildren(ctx)
    }

    @Override
    Boolean visitMake(GrammarParser.MakeContext ctx) {
        //Add children to file but add child nodes in between to file.
        processedFile.append(ctx.getChild(0)).append(" ").append(ctx.getChild(1)).append("\n")
        boolean result = visitChildren(ctx)
        processedFile.append(ctx.getChild(ctx.getChildCount() - 1)).append("\n")
        return result
    }

    @Override
    Boolean visitMaxmembergroup(GrammarParser.MaxmembergroupContext ctx) {
        //Simply add children to file.
        processedFile.append(ctx.getChild(0)).append(" ").append(ctx.getChild(1)).append("\n")
        return visitChildren(ctx)
    }

    @Override
    Boolean visitRemovemembers(GrammarParser.RemovemembersContext ctx) {
        //Add paranthesis to method with no arguments and add it to the file.
        String refactoredRemoveMembers = Refactory.refactorRemoveMembers(ctx.getChild(0).toString())
        processedFile.append(refactoredRemoveMembers).append("\n")
        return visitChildren(ctx)
    }

    @Override
    Boolean visitGrantuploadpermission(GrammarParser.GrantuploadpermissionContext ctx) {
        //Add paranthesis to method with no arguments and add it to the file.
        String refactoredUploadPermission = Refactory.refactorGrantUploadPermission(ctx.getChild(0).toString())
        processedFile.append(refactoredUploadPermission).append("\n")
        return visitChildren(ctx)
    }

    @Override
    Boolean visitRegistrationDates(GrammarParser.RegistrationDatesContext ctx) {
        if (validateRegistrationDates(ctx.getChild(1).toString().replaceAll(/"/, ""), ctx.getChild(3).toString().replaceAll(/"/, ""))) {
            processedFile.append(ctx.getChild(0)).append(" ").append(ctx.getChild(1)).append(" ").append(ctx.getChild(2)).append(ctx.getChild(3)).append("\n")
            return visitChildren(ctx)
        } else {
            return true
        }
    }

    @Override
    Boolean visitHelp(GrammarParser.HelpContext ctx) {
        processedFile.append(Refactory.refactorHelp())
        return visitChildren(ctx)
    }

    String getFile() {
        return processedFile.toString()
    }

    /**
     * Checks if the registration start and end date are valid and
     * if the start date is before the end date.
     * @param start The registration start date
     * @param end The registration end date
     * @return True if the registration start and end date are valid and
     * if the start date is before the end date.
     */
    private boolean validateRegistrationDates(String start, String end) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY)
        Calendar calendar = Calendar.getInstance()
        LocalDateTime startDate
        LocalDateTime endDate
        try {
            calendar.setTime(dateFormat.parse(start))
            startDate = LocalDateTime.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND))
        } catch (ParseException e) {
            Log.e("The registration start date is invalid.")
            return false
        }
        try {
            calendar.setTime(dateFormat.parse(end))
            endDate = LocalDateTime.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND))
        } catch (ParseException e) {
            Log.e("The registration end date is invalid")
            return false
        }
        if (startDate.isBefore(endDate)) {
            return true
        } else {
            Log.e("The registration start date must be before the end date")
            return false
        }
    }
}
