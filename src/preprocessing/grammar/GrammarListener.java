// Generated from C:/Users/Adrian/Projekte/groovywithilias\Grammar.g4 by ANTLR 4.7
package preprocessing.grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GrammarParser}.
 */
public interface GrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link GrammarParser#operation}.
	 * @param ctx the parse tree
	 */
	void enterOperation(GrammarParser.OperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#operation}.
	 * @param ctx the parse tree
	 */
	void exitOperation(GrammarParser.OperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#help}.
	 * @param ctx the parse tree
	 */
	void enterHelp(GrammarParser.HelpContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#help}.
	 * @param ctx the parse tree
	 */
	void exitHelp(GrammarParser.HelpContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#user}.
	 * @param ctx the parse tree
	 */
	void enterUser(GrammarParser.UserContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#user}.
	 * @param ctx the parse tree
	 */
	void exitUser(GrammarParser.UserContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#pw}.
	 * @param ctx the parse tree
	 */
	void enterPw(GrammarParser.PwContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#pw}.
	 * @param ctx the parse tree
	 */
	void exitPw(GrammarParser.PwContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#courses}.
	 * @param ctx the parse tree
	 */
	void enterCourses(GrammarParser.CoursesContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#courses}.
	 * @param ctx the parse tree
	 */
	void exitCourses(GrammarParser.CoursesContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#course}.
	 * @param ctx the parse tree
	 */
	void enterCourse(GrammarParser.CourseContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#course}.
	 * @param ctx the parse tree
	 */
	void exitCourse(GrammarParser.CourseContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#group}.
	 * @param ctx the parse tree
	 */
	void enterGroup(GrammarParser.GroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#group}.
	 * @param ctx the parse tree
	 */
	void exitGroup(GrammarParser.GroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#addGroup}.
	 * @param ctx the parse tree
	 */
	void enterAddGroup(GrammarParser.AddGroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#addGroup}.
	 * @param ctx the parse tree
	 */
	void exitAddGroup(GrammarParser.AddGroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#make}.
	 * @param ctx the parse tree
	 */
	void enterMake(GrammarParser.MakeContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#make}.
	 * @param ctx the parse tree
	 */
	void exitMake(GrammarParser.MakeContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#order}.
	 * @param ctx the parse tree
	 */
	void enterOrder(GrammarParser.OrderContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#order}.
	 * @param ctx the parse tree
	 */
	void exitOrder(GrammarParser.OrderContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#maxmembergroup}.
	 * @param ctx the parse tree
	 */
	void enterMaxmembergroup(GrammarParser.MaxmembergroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#maxmembergroup}.
	 * @param ctx the parse tree
	 */
	void exitMaxmembergroup(GrammarParser.MaxmembergroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#removemembers}.
	 * @param ctx the parse tree
	 */
	void enterRemovemembers(GrammarParser.RemovemembersContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#removemembers}.
	 * @param ctx the parse tree
	 */
	void exitRemovemembers(GrammarParser.RemovemembersContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#grantuploadpermission}.
	 * @param ctx the parse tree
	 */
	void enterGrantuploadpermission(GrammarParser.GrantuploadpermissionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#grantuploadpermission}.
	 * @param ctx the parse tree
	 */
	void exitGrantuploadpermission(GrammarParser.GrantuploadpermissionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#registrationDates}.
	 * @param ctx the parse tree
	 */
	void enterRegistrationDates(GrammarParser.RegistrationDatesContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#registrationDates}.
	 * @param ctx the parse tree
	 */
	void exitRegistrationDates(GrammarParser.RegistrationDatesContext ctx);
}