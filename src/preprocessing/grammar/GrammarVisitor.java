// Generated from C:/Users/Adrian/Projekte/groovywithilias\Grammar.g4 by ANTLR 4.7
package preprocessing.grammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GrammarParser#operation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperation(GrammarParser.OperationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#help}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHelp(GrammarParser.HelpContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#user}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUser(GrammarParser.UserContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#pw}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPw(GrammarParser.PwContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#courses}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCourses(GrammarParser.CoursesContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#course}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCourse(GrammarParser.CourseContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#group}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroup(GrammarParser.GroupContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#addGroup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddGroup(GrammarParser.AddGroupContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#make}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMake(GrammarParser.MakeContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#order}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrder(GrammarParser.OrderContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#maxmembergroup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaxmembergroup(GrammarParser.MaxmembergroupContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#removemembers}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRemovemembers(GrammarParser.RemovemembersContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#grantuploadpermission}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGrantuploadpermission(GrammarParser.GrantuploadpermissionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#registrationDates}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegistrationDates(GrammarParser.RegistrationDatesContext ctx);
}