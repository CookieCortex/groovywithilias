package preprocessing

import main.Log

import java.util.regex.Matcher
import java.util.stream.IntStream

class Refactory {

    private final static String RANGE_REGEX = /\[(\d+)-(\d+)]/
    private final static String ENUMERATION_REGEX = /\[(\w+)((,\w+)*)]/
    private final static String ANY_REGEX = /.*/

    /**
     * Escapes the '$' char in a given password.
     * @param password The password to escape
     * @return The escaped password
     */
    static String refactorPassword(String password) {
        return password.replace("\$", "\\\$")
    }

    static String refactorGrantUploadPermission(String permission) {
        return addParenthesisAtEnd(permission)
    }

    static String refactorRemoveMembers(String removeMembers) {
        return addParenthesisAtEnd(removeMembers)
    }

    static String refactorHelp() {
        return "help()"
    }

    /**
     * Maps a pseudo regular expression to correct a one.
     * The pseudo expressions can be:
     *      [d1-d2], d1 and d2 can be any positive integer, with d1 <= d2
     *      [w1,w2,w3,...], wi can be any word
     *
     * This mapping is needed, because the pseudo regular expressions are not
     * correct regular expressions. The pseudo ones are provided to the user for
     * usability reasons.
     * @param element The  pseudo regular expression
     * @return A correct regular expressions, which means the same as the pseudo one or null if an error occurred while
     *         refactoring the regular expression
     */
    static String refactorGroupElement(String element) {
        element = replaceRangeRegex(element)
        element = replaceEnumerationRegex(element)
        element = replaceAnyRegex(element)
        return element
    }

    /**
     * Transforms a given group name using a pseudo regex in valid elements.
     * There must be only one regex usage in the name.
     * Possibilities are:
     *      myGroupName[8-11] to
     *          myGroupName8
     *          myGroupName9
     *          myGroupName10
     *          myGroupName11
     * @param element The name with the possible pseudo redex
     * @return A list with the transformed elements
     */
    static ArrayList<String> refactorAddGroupElement(String element) {
        element = removePredefinedCharacters(element)
        ArrayList<String> result = expandRangeRegex(element)
        if (result != null && result.size() == 1) {
            result = expandEnumerationRegex(element)
        }
        return result
    }

    /**
     * Removes predefined characters from a given string.
     * TODO: There may be a better way to handle predefined symbols.
     * @param element The string to check
     * @return The stripped string.
     */
    private static String removePredefinedCharacters(String element) {
        element = element.replaceAll(/\*/, "")
        element = element.replaceAll(/\?/, "")
        element = element.replaceAll(/\+/, "")
        element = element.replaceAll(/\|/, "")
        return element
    }

    private static String replaceAnyRegex(String anyRegex) {
        if(anyRegex == null) {
            return null
        }
        return anyRegex.replaceAll(/\*/, ANY_REGEX)
    }

    /**
     * Transforms a given pseudo regex into a valid one.
     * For example myGroupName[aBc,2,s3] to myGroupName(aBc|2|s3)
     * @param enumRegex THe pseudo regex to transform
     * @return The valid regex
     */
    private static String replaceEnumerationRegex(String enumRegex) {
        if(enumRegex == null) {
            return null
        }
        Matcher matcher = (enumRegex =~ ENUMERATION_REGEX)
        while (matcher.find()) {
            String subStrings = matcher.group().replace('[', '(').replace(']', ')').replace(",", "|")
            enumRegex = enumRegex.replaceFirst(ENUMERATION_REGEX, subStrings + "")
        }
        return enumRegex
    }

    /**
     * Transforms a given pseudo regex into a valid one.
     * For example myGroupName[8-14] to myGroupName(8|9|10|11|12|13|14)
     * @param rangeRegex The pseudo regex
     * @return The valid regex or null if an error occurred.
     */
    private static String replaceRangeRegex(String rangeRegex) {
        if(rangeRegex == null) {
            return null
        }
        Matcher matcher = (rangeRegex =~ RANGE_REGEX)
        while (matcher.find()) {
            //TODO: Maybe use algorithm to define range in valid regex
            StringBuilder stringBuilder = new StringBuilder("(")
            String[] subStrings = matcher.group().replace('[', '').replace(']', '').split('-')
            int from = subStrings[0].toInteger()
            int to = subStrings[1].toInteger()
            if(to < from) {
                Log.e("The range of a regular expression cannot go from $to to $from")
                return null
            }
            IntStream.range(from, to + 1).forEach({ rangeValue -> stringBuilder.append(rangeValue).append("|") })
            stringBuilder.deleteCharAt(stringBuilder.length() - 1).append(")")
            rangeRegex = rangeRegex.replaceFirst(RANGE_REGEX, stringBuilder.toString())
        }
        return rangeRegex
    }

    /**
     * Transform a given enumeration regex to individual elements.
     * For example
     *      myGroupName[aBc,2,s3] to
     *      myGroupNameaBc
     *      myGroupName2
     *      myGroupNames3
     * The individual elements are stored in a list.
     * @param enumRegex The pseudo regex to transform
     * @return A list with the individual elements.
     */
    private static ArrayList<String> expandEnumerationRegex(String enumRegex) {
        if(enumRegex == null ) {
            return null
        }
        ArrayList<String> result = new ArrayList<>()
        Matcher matcher = (enumRegex =~ ENUMERATION_REGEX)
        if (matcher.find()) {
            String[] subStrings = matcher.group().replace('[', '').replace(']', '').split(',')
            for (String subString : subStrings) {
                result.add(enumRegex.replaceFirst(ENUMERATION_REGEX, subString))
            }
        } else {
            result.add(enumRegex)
        }
        return result
    }

    /**
     * Transforms a given pseudo range regex to individual elements.
     * For example
     *      myGroupName[9-13] to
     *      myGroupName9
     *      myGroupName10
     *      myGroupName11
     *      myGroupName12
     *      myGroupName13
     * The individual elements are stored in a list.
     * @param rangeRegex The pseudo regex to transform
     * @return A list with the individual valid elements or null if an error occurred
     */
    private static ArrayList<String> expandRangeRegex(String rangeRegex) {
        if(rangeRegex == null) {
            return null
        }
        ArrayList<String> result = new ArrayList<>()
        Matcher matcher = (rangeRegex =~ RANGE_REGEX)
        if (matcher.find()) {
            String[] subStrings = matcher.group().replace('[', '').replace(']', '').split('-')
            int from = subStrings[0].toInteger()
            int to = subStrings[1].toInteger()
            if(to < from) {
                Log.e("The range of a regular expression cannot go from $to to $from")
                return null
            }
            IntStream.range(from, to + 1).forEach({ rangeValue -> result.add(rangeRegex.replaceFirst(RANGE_REGEX, rangeValue + "")) })
        } else {
            result.add(rangeRegex)
        }
        return result
    }

    /**
     * Adds parenthesis at the end of a string iff not already added.
     * @param input The string which is meant to be a single word representing a method with no parameters.
     * @return The string with the added parenthesis
     */
    private static addParenthesisAtEnd(String input) {
        if (!(input ==~ /.*\(\)$/)) {
            return input + "()"
        }
        return input
    }
}
