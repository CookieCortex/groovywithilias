package ilias

import com.github.mavogel.ilias.lib.EndpointBuilder
import com.github.mavogel.ilias.lib.model.IliasNode
import com.github.mavogel.ilias.lib.model.LoginConfiguration
import com.github.mavogel.ilias.lib.model.UserDataIds
import com.github.mavogel.ilias.lib.wrapper.AbstractIliasEndpoint
import com.github.mavogel.ilias.lib.wrapper.DisplayStatus
import com.github.mavogel.ilias.utils.IOUtils
import main.Log

import java.time.LocalDateTime
import java.util.function.Predicate
import java.util.stream.Collectors

/**
 * A facade to the functionality of the ilias interface.
 */
class Connector {

    /*The endpoint and the client information to connect to the ilias system.*/
    private final static String ENDPOINT = "https://ilias.hs-karlsruhe.de/webservice/soap/server.php"
    private final static String CLIENT = "HSKA"
    /*You can also use the following connection information to connect to the test ilias system.
    However you need a user account on this test system (Your normal ilias account isn't registered there).
    To get such an account you need to talk to Mr. Schroeder from the IZ-administration.*/
    private final static String DEBUG_ENDPOINT = "https://ilias2.hs-karlsruhe.de/ilias44/webservice/soap/server.php"
    private final static String DEBUG_CLIENT = "ilias44"
    private final static int MAX_FOLDER_DEPTH = 5
    private final static int MAXIMUM_AMOUNT_OF_NEW_GROUPS_TO_ADD = 20

    private AbstractIliasEndpoint iliasEndpoint
    private UserDataIds userData
    private String userName
    private String password
    private boolean loggedIn = false

    /**
     * Establishes a connection to the ilias system and logs in a given user.
     */
    void connect() {
        if (!loggedIn) {
            requestPasswordIfNeeded()
            LoginConfiguration loginConfiguration = LoginConfiguration.asLDAPLogin(ENDPOINT, CLIENT, userName, password, MAX_FOLDER_DEPTH)
            try {
                iliasEndpoint = EndpointBuilder.build(EndpointBuilder.Type.SOAP, loginConfiguration)
                userData = iliasEndpoint.getUserDataIds()
            } catch (Exception e) {
                Log.e("Could not login user. Program stops.")
                loggedIn = false
                System.exit(0)
            } finally {
                if (userData != null && userData.getSid() != null) {
                    loggedIn = true
                    Log.i("Login was successful")
                }
            }
        } else {
            Log.d("Already logged in user")
        }
    }

    /**
     * Searches for a course, the user is admin of. If multiple courses
     * are found, the first one will be picked. TODO: Maybe ask user to choose?
     * @param name The name of the course to search for
     * @return An {@link IliasNode} representing this course
     */
    IliasNode findCourse(String name) {
        List<IliasNode> result = getCoursesOfUser().stream().filter(new Predicate<IliasNode>() {
            @Override
            boolean test(IliasNode iliasNode) {
                return iliasNode.title.equals(name)
            }
        }).collect(Collectors.toList())
        return result.get(0)
    }

    /**
     * Retrieves a list with all groups of a given course
     * @param course The course
     * @return A list with the groups
     */
    List<IliasNode> getGroupsOfCourse(IliasNode course) {
        List<IliasNode> groupsOfCourse = new ArrayList<>()
        try {
            groupsOfCourse = iliasEndpoint.getGroupsFromCourse(course)
        } catch (Exception e) {
            e.printStackTrace()
        }
        return groupsOfCourse
    }

    void logout() {
        if (userData != null && userData.getSid() != null) {
            iliasEndpoint.logout("")
        }
    }

    /**
     * Retrieves a list with all courses of the logged in user.
     * @return The list with all owned courses
     */
    private List<IliasNode> getCoursesOfUser() {
        List<IliasNode> coursesOfUser = new ArrayList<>()
        try {
            coursesOfUser = iliasEndpoint.getCoursesForUser(DisplayStatus.ADMIN)
        } catch (Exception e) {
            e.printStackTrace()
        }
        return coursesOfUser
    }

    /**
     * Creates new groups in a given course folder.
     * @param course The course of the new groups
     * @param groupNames The names of the groups
     * @return A list with all the groups that have successfully been created.
     */
    ArrayList<String> addGroups(IliasNode course, String[] groupNames) {
        ArrayList<String> createdGroupNames = new ArrayList<>()
        if (!(checkAmountOfNewGroups(groupNames))) {
            Log.i("Created 0 new groups")
            return createdGroupNames
        }
        for (String groupName : groupNames) {
            groupName = groupName.trim()
            if (groupName.isEmpty()) {
                Log.i("Group name cannot be empty")
                continue
            }
            if (addGroup(course, groupName)) {
                createdGroupNames.add(groupName)

            }
        }
        Log.i("Created " + createdGroupNames.size() + " new groups")
        return createdGroupNames
    }

    void grantFileUploadPermissionForMembers(List<IliasNode> groups) {
        iliasEndpoint.grantFileUploadPermissionForMembers(groups)
    }

    void removeAllMembersFromGroups(List<IliasNode> groups) {
        iliasEndpoint.removeAllMembersFromGroups(groups)
    }

    void setMaxMembersOnGroups(List<IliasNode> groups, int max) {
        iliasEndpoint.setMaxMembersOnGroups(groups, max)
    }

    void setRegistrationDatesOnGroups(List<IliasNode> groups, LocalDateTime start, LocalDateTime end) {
        iliasEndpoint.setRegistrationDatesOnGroups(groups, start, end)
    }

    void setUserName(String userName) {
        this.userName = userName
    }

    void setPassword(String password) {
        this.password = password
    }

    /**
     * Adds a group with a given name to a given course.
     * If the group already exists, the user will be asked if he still wants to create the group.
     * @param course The course to add the group to
     * @param groupName The name of the new group
     * @return True , iff the group has been successfully added
     */
    private boolean addGroup(IliasNode course, String groupName) {
        if (iliasEndpoint.groupExists(groupName)) {
            Log.i("Group \"" + groupName + "\" already exists, but unfortunately we cannot check in which course. Please check it yourself. Do you still want to create this group?");
            if (IOUtils.readAndParseUserConfirmation()) {
                return iliasEndpoint.addGroup(course, groupName)
            } else {
                return false
            }
        } else {
            return iliasEndpoint.addGroup(course, groupName)
        }
    }

    /**
     * If no password has been provided via the input script,
     * then it is requested via console separately.
     */
    private void requestPasswordIfNeeded() {
        if (password == null || password.isEmpty()) {
            Console console = System.console()
            if (console != null) {
                password = String.valueOf(console.readPassword("Enter your password: "))
            } else {
                throw new Exception("Console is not available!")
            }
        }
    }

    /**
     * Checks if more than {@code MAXIMUM_AMOUNT_OF_NEW_GROUPS_TO_ADD} are added.
     * If yes the user will be asked if he wants to continue.
     * @param newGroupNames The names of the new groups
     * @return true if less than {@code MAXIMUM_AMOUNT_OF_NEW_GROUPS_TO_ADD} groups are added
     *              or if more are added and the user wants to continue
     */
    private boolean checkAmountOfNewGroups(String[] newGroupNames) {
        if (newGroupNames.length >= MAXIMUM_AMOUNT_OF_NEW_GROUPS_TO_ADD) {
            Log.i("You are about to add $newGroupNames.length new groups. This can take a lot of time. Are you sure you want to continue?")
            return IOUtils.readAndParseUserConfirmation()
        } else {
            return true
        }
    }
}
