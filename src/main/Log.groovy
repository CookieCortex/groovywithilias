package main

import org.apache.log4j.Logger

class Log {

    private static Logger LOG = Logger.getLogger(Log.class);
    static boolean SHOW_DEBUG = false

    static void i(String message) {
        LOG.info(message)
    }

    static void d(String message) {
        if (SHOW_DEBUG) {
            println(message)
        }
    }

    static void e(String message) {
        LOG.error(message)
    }

}
