package main

import dsl.DSLLogic
import dsl.DSLLogicScript
import ilias.Connector
import org.codehaus.groovy.control.CompilerConfiguration
import preprocessing.Preprocessor

class MainGroovy {
    static void main(String... args) {
        println("\n\n")
        Log.i("Starting Script.")
        Log.SHOW_DEBUG = false
        Connector connector = new Connector()
        DSLLogic dslLogic = new DSLLogic(connector)
        //Pre process script
        File file = new File(args[0])
        String preprocessedFile = Preprocessor.preprocessFile(file)
        if (preprocessedFile != null) {
            // Configure the GroovyShell.
            CompilerConfiguration compilerConfiguration = new CompilerConfiguration()
            compilerConfiguration.scriptBaseClass = DSLLogicScript.class.name
            // Add to script binding (DSLLogicScript references this.binding.dsl).
            Binding binding = new Binding(dsl: dslLogic)
            GroovyShell shell = new GroovyShell(this.class.classLoader, binding, compilerConfiguration)
            // Run DSL script.
            shell.evaluate(preprocessedFile)
        }
        connector.logout()
        Log.i('Script finished')
        println("\n")
    }
}
