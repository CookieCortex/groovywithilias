package main

class Printer {

    static void printGrammar() {
        //TODO printf file
        println(
                "grammar Grammar;\n" +
                        "operation\n" +
                        ": user pw? courses+ | help\n" +
                        ";\n" +

                        "help\n" +
                        ": 'help'\n" +
                        ";\n" +

                        "user\n" +
                        ": 'user' STRING\n" +
                        ";\n" +

                        "pw\n" +
                        ": 'pw' STRING\n" +
                        ";\n" +

                        "courses\n" +
                        ": course (group? | addGroup) make\n" +
                        ";\n" +

                        "course\n" +
                        ": 'course' STRING\n" +
                        ";\n" +

                        "group\n" +
                        ": 'group' (STRING',')* STRING\n" +
                        ";\n" +

                        "addGroup\n" +
                        ": 'addGroup' (STRING',')* STRING\n" +
                        ";\n" +

                        "make\n" +
                        ": 'make' '{' (order)+'}'\n" +
                        ";\n" +

                        "order\n" +
                        ": maxmembergroup | removemembers | grantuploadpermission | registrationDates\n" +
                        ";\n" +

                        "maxmembergroup\n" +
                        ": 'maxMemberPerGroup' INTEGER\n" +
                        ";\n" +

                        "removemembers\n" +
                        ": 'removeMembers'\n" +
                        ";\n" +

                        "grantuploadpermission\n" +
                        ": 'grantUploadPermission'\n" +
                        ";\n" +

                        "registrationDates\n" +
                        ": 'registrationDates' STRING',' STRING\n" +
                        ";\n" +

                        "INTEGER\n" +
                        ": [0-9]+\n" +
                        ";\n" +

                        "STRING\n" +
                        ": '\"'[a-zA-Z0-9!#\$%&'() *,-./:;<=>?@[\\]^_`{|}~\\\\+]+'\"'\n" +
                        ";\n" +

                        "WHITESPACE\n" +
                        ": (' ' | '\\t' | '\\n' | '\\r')+ -> skip\n" +
                        ";")
    }

    static void printExampleScript() {
        println("Example 1: Script to create the groups \"team1\" and \"team2\" with a registration period.")
        println("user \"myUserName\"")
        println("pw \"myPassword\"")
        println("course addGroups \"team1\",\"team2\" make {")
        println("   registrationDates \"04.10.20017\", \"28.02.2018\"")
        println("}\n")

        println("Example 2: Script to create the groups \"team9\", \"team10\", \"team11\" and \"teamA\" and allow them to upload files.")
        println("user \"myUserName\"")
        println("pw \"myPassword\"")
        println("course addGroups \"team[9-10]\",\"team[11|A]\" make {")
        println("   grantUploadPermission")
        println("}\n")

        println("Example 3: Script to select the groups \"team7 type A\", \"team7 type B\", \"team8 type A\" and \"team8 type B\" with a maximum number of member of 7.")
        println("user \"myUserName\"")
        println("pw \"myPassword\"")
        println("course group \"team[7-8] type [A|B|\",\"team2\" make {")
        println("   maxMemberPerGroup 7")
        println("}\n")
    }

    static void printMethodDescription() {
        println("Methods and a description of them:")
        println("- addGroups String... groupNames: A method to create new groups/courses.")
        println("- grantUploadPermission: Allows the members of the selected groups to upload files.")
        println("- removeMembers: Removes all members of the selected groups")
        println("- maxMemberPerGroup int maximum: Changes the maximum amount of members per group for all selected groups.")
        println("- registrationDates String start, String end : Specifies the the period to join the group.")
        println("- help: Call the help function which shows useful information.")
    }

    static void printGeneralDescription() {
        println("General Description:")
        println("GroovyWithIlias is a project made by students of the Hochschule Karlsruhe. It is meant to " +
                "simplify repetitive and cumbersome tasks on the learning platform Ilias. It builds upon the " +
                "Ilias-Client from Manuel Vogel which is written in Java. The extension is " +
                "written in the scripting language Groovy. This allows you to call the methods written in " +
                "Groovy by using a script and for example easily create new courses in Ilias or change the " +
                "maximum number of members of an existing  course. It should be noted that the available " +
                "methods are mainly focused on changing courses. ")
    }

    static void printAbout() {
        println("We are two students at the Hochschule Karlsruhe - Technik und Wirtschaft (HsKA), who created " +
                "the project GroovyWithIlias for a project work within the framework of our computer science " +
                "studies. The project work ranged from the beginning of October 2017 to the end of February 2018 " +
                "and was supervised by the lecturer Prof. Dr. Zirpins. The project work has been successfully " +
                "completed and the version of the project considered here corresponds to the final version " +
                "developed by us. ")
        println("")
        println("Written by Alexander Hoffmann and Adrian Wörle")
    }
}
