package dsl

import com.github.mavogel.ilias.lib.model.IliasNode
import ilias.Connector
import main.Log
import main.Printer

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.function.Predicate
import java.util.stream.Collectors

/**
 * Serves as binding class to the {@link DSLLogicScript} class.
 * Handles the method calls of the script.
 */
class DSLLogic {

    private IliasNode currentCourse
    private List<IliasNode> currentGroups
    private Connector connector

    DSLLogic(Connector connector) {
        this.connector = connector
    }

    void setUser(String username) {
        connector.userName = username
    }

    void setPassword(String password) {
        connector.password = password
    }

    /**
     * Sets the current course. This also establishes a connection to
     * the ilias system. Therfore, when this method is called, the username and the password
     * must be already set.
     * @param name The name of the course
     */
    void setCurrentCourse(String name) {
        connector.connect()
        currentCourse = connector.findCourse(name)
    }

    /**
     * Creates a new group in the current course
     * for each provided name in the ilias system and
     * sets all groups that have been successfully been created as
     * the current groups.
     * @param groupNames The names of the new groups
     */
    void addGroup(String[] groupNames) {
        groupNames = groupNames.toUnique()
        groupNames = connector.addGroups(currentCourse, groupNames)
        loadGroups(groupNames)
    }

    void setCurrentGroups(String[] names) {
        loadGroups(names)
    }

    void grantUploadPermission() {
        connector.grantFileUploadPermissionForMembers(currentGroups)
    }

    void removeMembers() {
        connector.removeAllMembersFromGroups(currentGroups)
    }

    void maxMemberPerGroup(int max) {
        connector.setMaxMembersOnGroups(currentGroups, max)
    }

    void showHelp() {
        int SHOW_GRAMMAR = 1
        int SHOW_EXAMPLE = 2
        int SHOW_METHOD_DESCRIPTION = 3
        int SHOW_PROGRAM_DESCRIPTION = 4
        int SHOW_ABOUT = 5
        int EXIT = 6

        println("Help Options:")
        println("1. Show grammar")
        println("2. Example of a script")
        println("3. Description of the methods")
        println("4. General description of the program")
        println("5. About us")
        println("6. Exit program")
        println("Please enter the number of the option you want to use:")
        Scanner scanner = new Scanner(System.in)
        int chosenOption = -1
        while (chosenOption != 6) {
            switch (scanner.nextInt()) {
                case SHOW_GRAMMAR:
                    Printer.printGrammar()
                    break
                case SHOW_EXAMPLE:
                    Printer.printExampleScript()
                    break
                case SHOW_METHOD_DESCRIPTION:
                    Printer.printMethodDescription()
                    break
                case SHOW_PROGRAM_DESCRIPTION:
                    Printer.printGeneralDescription()
                    break
                case SHOW_ABOUT:
                    Printer.printAbout()
                    break
                case EXIT:
                    System.exit(0)
                    break
            }
        }
    }

    /**
     * Sets the registration period of the current groups.
     * The dates have already been validated in the pre processing step.
     * @param start The start registration date
     * @param end The end registration date
     */
    void registrationDates(String start, String end) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY)
        Calendar calendar = Calendar.getInstance()
        calendar.setTime(dateFormat.parse(start))
        LocalDateTime startDate = LocalDateTime.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND))
        calendar.setTime(dateFormat.parse(end))
        LocalDateTime endDate = LocalDateTime.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND))
        connector.setRegistrationDatesOnGroups(currentGroups, startDate, endDate)
    }

    /**
     * Loads all groups of the current course
     * that match one of the given names.
     * @param names The names of the groups to load
     */
    private void loadGroups(String[] names) {
        //Because all operations are on groups, we can load them
        //all at the beginning. If the user wants only some of them
        //they need to be filtered out. Right now it is not possible
        //to only query some groups of a course.
        currentGroups = connector.getGroupsOfCourse(currentCourse)
        filterCurrentGroups(names)
        if(currentGroups.isEmpty()) {
            Log.i("Found no groups to work with.")
        }
    }

    /**
     * Filters out all groups of the current course, that don't match
     * one of the given names.
     * @param names The given names to filter the current groups by. These
     *              names can contain valid regular expressions.
     */
    private void filterCurrentGroups(String[] names) {
        currentGroups = currentGroups.stream().filter(new Predicate<IliasNode>() {
            @Override
            boolean test(IliasNode iliasNode) {
                boolean contains = false
                for (String name : names) {
                    contains |= (iliasNode.title.toString() ==~ name)
                }
                return contains
            }
        }).collect(Collectors.toList())
    }
}
