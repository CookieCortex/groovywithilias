package dsl

/**
 * Serves as script which is called from the GroovyShell.
 * The methods are called, when they are recognized in a given input script.
 */
abstract class DSLLogicScript extends Script {

    DSLLogicScript user(String name) {
        this.binding.dsl.setUser(name)
    }

    DSLLogicScript pw(String name) {
        this.binding.dsl.setPassword(name)
    }

    DSLLogicScript course(String name) {
        this.binding.dsl.setCurrentCourse(name)
        return this
    }

    DSLLogicScript group(String... name) {
        this.binding.dsl.setCurrentGroups(name)
        return this
    }

    DSLLogicScript addGroup(String... groupNames) {
        this.binding.dsl.addGroup(groupNames)
        return this
    }

    def make(Closure c) {
        c()
    }

    def grantUploadPermission() {
        this.binding.dsl.grantUploadPermission()
    }

    def removeMembers() {
        this.binding.dsl.removeMembers()
    }

    def maxMemberPerGroup(int maximum) {
        this.binding.dsl.maxMemberPerGroup(maximum)
    }

    def registrationDates(String start, String end) {
        this.binding.dsl.registrationDates(start, end)
    }

    def help() {
        this.binding.dsl.showHelp()
    }
}
