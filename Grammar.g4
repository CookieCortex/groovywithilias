grammar Grammar;

operation 
    : user pw? courses+ | help
    ;

help
    : 'help'
    ;

user 
    : 'user' STRING
    ;

pw 
    : 'pw' STRING
    ;

courses 
    : course (group? | addGroup) make
    ;

course 
    : 'course' STRING
    ;

group 
    : 'group' (STRING',')* STRING
    ;

addGroup
    : 'addGroup' (STRING',')* STRING
    ;

make 
    : 'make' '{' (order)+'}'
    ;

order 
    : maxmembergroup | removemembers | grantuploadpermission | registrationDates
    ;

maxmembergroup 
    : 'maxMemberPerGroup' INTEGER
    ;

removemembers
    : 'removeMembers'
    ;

grantuploadpermission 
    : 'grantUploadPermission'
    ;

registrationDates 
    : 'registrationDates' STRING',' STRING
    ;

INTEGER 
    : [0-9]+
    ;

STRING 
    : '"'[a-zA-Z0-9!#$%&'() *,-./:;<=>?@[\]^_`{|}~\\+]+'"'
    ;

WHITESPACE 
    : (' ' | '\t' | '\n' | '\r')+ -> skip 
    ;

